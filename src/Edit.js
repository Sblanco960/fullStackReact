import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  Pressable,
  Modal,
} from "react-native";

const EditUser = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [users, setUsers] = useState([]);
  const [userId, setUserId] = useState("");
  const [nameEdit, setNameEdit] = useState("");
  const [emailEdit, setEmailEdit] = useState("");

  let headers = {
    "Content-type": "application/json; charset=UTF-8",
    Authorization: `Bearer`,
  };

  useEffect(() => {
    const getUsers = () => {
      axios
        .get("http://127.0.0.1:8000/api/users", { headers })

        .then((resp) => {
          setUsers(resp.data);
        });
    };

    getUsers();
  }, [saveEditUser]);

  const handleUserEdit = (dataUser) => {
    setModalVisible(true);
    setNameEdit(dataUser.name);
    setEmailEdit(dataUser.email);
    setUserId(dataUser.id);
  };

  const saveEditUser = async () => {
    await axios
      .post(
        "http://127.0.0.1:8000/api/users/update",
        { id: userId, name: nameEdit, email: emailEdit },
        { headers }
      )
      .then((resp) => {
        setModalVisible(false);
        let auxUsers = [];
        users.map((user) => {
          if (user.id == userId) {
            user.name = nameEdit;
            user.email = emailEdit;
          }
          auxUsers.push(user);
        });
        setUsers(auxUsers);
        alert("Usuario Editado Correctamente");
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <View style={styles.container}>
      <Text>Usuarios</Text>
      <View style={styles.table}>
        <View style={styles.row}>
          <Text style={styles.cell}>Id</Text>
          <Text style={styles.cell}>Nombre</Text>
          <Text style={styles.cell}>Email</Text>
          <Text style={styles.cell}>Acción</Text>
        </View>
        {users.length > 0
          ? users.map((user) => (
              <View style={styles.row}>
                <Text style={styles.cell}>{user.id}</Text>
                <Text style={styles.cell}>{user.name}</Text>
                <Text style={styles.cell}>{user.email}</Text>
                <Pressable
                  style={styles.button}
                  onPress={() => handleUserEdit(user)}
                >
                  <Text style={styles.text}>Editar</Text>
                </Pressable>
              </View>
            ))
          : ""}
      </View>
      <Modal
        animationType="slide" // Opciones: "slide", "fade", "none"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
        sty
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.label}>Nombre</Text>
            <TextInput
              name="name"
              style={styles.input}
              value={nameEdit}
              onChangeText={(text) => setNameEdit(text)}
            />

            <Text style={styles.label}>Correo electrónico</Text>
            <TextInput
              style={styles.input}
              value={emailEdit}
              keyboardType="email-address"
              onChangeText={(text) => setEmailEdit(text)}
            />
            <Button title="Actualizar Usuario" onPress={() => saveEditUser()} />
            <Button title="Cerrar" onPress={() => setModalVisible(false)} />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: "center",
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    marginBottom: 20,
    borderRadius: 9,
  },

  button: {
    borderRadius: 4,
    elevation: 3,
    backgroundColor: "black",
    flex: 1,
    padding: 1,
    textAlign: "center",
    fontSize: 2,
  },
  text: {
    fontSize: 11,
    lineHeight: 25,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "white",
    textAlign: "center",
  },
  table: {
    borderWidth: 1,
    borderColor: "#ccc",
  },
  row: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#ccc",
    textAlign: "center",
  },
  cell: {
    flex: 1,
    padding: 10,
    textAlign: "center",
    fontSize: 12,
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "white",
    padding: 20,
    borderRadius: 10,
    elevation: 5,
  },
});

export default EditUser;
