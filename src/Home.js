// HomeScreen.js
import axios from 'axios'
import React, { useEffect,useState } from 'react';
import { View, Button,StyleSheet,Pressable,Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const HomeScreen = ({ navigation }) => {

  const [token, setToken] = useState();


  useEffect(()=>{

      const getToken = async () => {
          let tokenGet = await AsyncStorage.getItem('access_token');            
          setToken(JSON.parse(tokenGet))           
      }

      getToken();        
      
  });

  console.log('tokenHome',token)

  let headers = {
    "Content-type": "application/json; charset=UTF-8",
  };

  const handleLogout = () => {
    AsyncStorage.removeItem("access_token");
    navigation.navigate('Login')
    console.log('hola2')
  }
 
  return (
    <View style={styles.container}>
                  
      <Pressable style={styles.button} onPress={()=>navigation.navigate('Login')}>
      <Text style={styles.text}>Login</Text>
      </Pressable>
      <Pressable style={styles.button} onPress={()=>navigation.navigate('Editar')}>
        <Text style={styles.text}>Editar Usuario</Text>
      </Pressable>
      <Pressable style={styles.button} onPress={()=>navigation.navigate('Registro')}>
        <Text style={styles.text}>Registrar Usuario</Text>
      </Pressable>
      <Pressable style={styles.button} onPress={()=>navigation.navigate('Mapa')}>
        <Text style={styles.text}>Mapa</Text>
      </Pressable>                                      
      
    </View>
  );

};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
    marginBottom:14,
    width:300
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },

});

export default HomeScreen;
