import axios from 'axios'
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet,Pressable,Card } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {

    
    const [token, setToken] = useState(undefined);


    useEffect(()=>{

        const getToken = async () => {
            let tokenGet = await AsyncStorage.getItem('access_token');            
            setToken(JSON.parse(tokenGet))           
        }

        getToken();        
        
    });


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');      
    const [value,setValue] = useState(0)
  

    let headers = {
        "Content-type": "application/json; charset=UTF-8",
    };

    const handleLogin = () => {
           
    axios.post('http://127.0.0.1:8000/api/login',{email,password})
    .then(resp=>{                
        if (resp.data.resp == '200') {            
            alert('Inicio de Sesión Correctamente !! ')                        
            AsyncStorage.setItem("access_token",JSON.stringify(resp.data.access_token));     
            setValue(value+1)                   
        }        
    }) 
    .catch((error) => {     
        console.error(error);
        alert('Verificar Credenciales de Acceso')
    });
   
  };
  
  const handleLogout = () => {
    AsyncStorage.removeItem("access_token");
    navigation.navigate('Inicio')
    console.log('hola') 
  }
  

  return (
    <View style={styles.container}>

        {
            value == 0 && token == undefined ? 
        
                <>                
                    <Text style={styles.label}>Correo electrónico</Text>
                    <TextInput
                        style={styles.input}
                        value={email}
                        onChangeText={text => setEmail(text)}
                        keyboardType="email-address"
                    />

                    <Text style={styles.label}>Contraseña</Text>
                    <TextInput
                        style={styles.input}
                        value={password}
                        onChangeText={text => setPassword(text)}
                        secureTextEntry
                    />
                        
                    <Pressable style={styles.button} onPress={handleLogin}>
                        <Text style={styles.text}>Ingresar</Text>
                    </Pressable>
                </>                

            :
                <>
                    <Pressable style={styles.button} onPress={handleLogout}>
                        <Text style={styles.text}>Cerrar Sesión</Text>
                    </Pressable>
                    <Pressable style={styles.button} onPress={()=>navigation.navigate('Registro')}>
                        <Text style={styles.text}>Registro Usuario</Text>
                    </Pressable>
                    <Pressable style={styles.button} onPress={()=>navigation.navigate('Editar')}>
                        <Text style={styles.text}>Editar Usuario</Text>
                    </Pressable>
                    <Pressable style={styles.button} onPress={()=>navigation.navigate('Mapa')}>
                        <Text style={styles.text}>Mapa</Text>
                    </Pressable>
                </>
            

      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginBottom: 20,
    borderRadius:9
  },

  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
    marginBottom:14,
    width:300
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
});

export default Login;